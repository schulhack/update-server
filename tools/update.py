#!/usr/bin/python3

import shutil
import json
import os
import re
import colorama


def fmt_i(question):
    return colorama.Fore.LIGHTBLUE_EX + question + colorama.Fore.RESET
    
def fmt_w(question):
    return colorama.Fore.YELLOW + question + colorama.Fore.RESET

try:
    versionCode = int(input(fmt_i("versionCode ▶ ")))
except ValueError:
    print(colorama.Fore.RED + "☝️ Version Code must be a number")
    exit(1)

versionName = input(fmt_i("versionName ▶ "))

if not re.match("^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(-(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(\.(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*)?(\+[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*)?$", versionName):
    print(colorama.Fore.RED + "☝️ That does not look like semver")
    exit(1)
# versionName = versionName.replace(".", "-")

baseUrl = input(fmt_i("Download server base url ▶ "))
if not re.match("^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$", baseUrl):
    print(colorama.Fore.RED + "☝️ That does not look like a valid URL")
    exit(1)


data = {}
abis = [
    "armeabi-v7a", "x86", "arm64-v8a", "x86_64"
]

for i in range(4):
    abi = i + 1
    abi_name = abis[i]
    abiVersionCode = abi * 1048576 + versionCode
    data[abi_name] = {
        "versionCode": abiVersionCode,
        "simpleVersionCode": versionCode,
        "versionName": versionName,
        "url": f"{baseUrl}/files/{versionCode}/schulhack-{versionName.replace('.', '-')}-{abi_name}-release.apk"
    }

public_dir = (os.path.realpath(__file__).split("/")[:-2])
public_dir.append("public")
public_dir = "/".join(public_dir)

shutil.copy(f"{public_dir}/versions.json", f"{public_dir}/versions.json.old")
with open(f"{public_dir}/versions.json", 'w') as file:
    print(colorama.Fore.GREEN + "Updating versions.json...")
    file.write(json.dumps(data))

print(fmt_w("Don't forget to update release_notes.json"))
